import React, { Component } from 'react';
import RouterLogin from './components/login/routerLogin';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import 'typeface-roboto';

class App extends Component {
  constructor() {
    super();
    this.routerState = {
      link: "",
      component: "login"
    };
  }

  render() {
    return (
      <BrowserRouter>
        <RouterLogin
          paramsRouter= {this.routerState}
        />
      </BrowserRouter>
    );
  }
}

export default App;
