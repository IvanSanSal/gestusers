import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';

import './newUser.css';

class NewUserForm extends Component {
  constructor() {
    super();

    this.state = {
      email: "",
      name: "",
      password: "",
      confirmPassword: "",
      birthday: "2000-01-01",
      gender: "male",      
    };

    this.validationForm = {
      email: false,
      name: false,
      password: false,
      confirmPassword: false
    }
  }

  createUser() {
    const validation = this.validationFilds();
    if(validation) {
      alert("Usuario registrado");
    } else {
      alert("Formulario incompleto");
    }
  }

  validateEmail(email) {
    let re = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
    return re.test(email);
  }

  validationFilds() {
    if (this.state.email) {
      //Validate valid email
      this.validationForm.email = !this.validateEmail(this.state.email);
    } else {
      //Validate Email    
      this.validationForm.email = !this.state.email ? true : false;    
    }        
    //Validation Name
    this.validationForm.name = !this.state.name ? true : false;    
    //Validation password
    this.validationForm.password = !this.state.password ? true : false;
    //Validation confirm Password
    this.validationForm.confirmPassword =!this.state.confirmPassword ? true : false;
    //Validation password and confirmPassword !=
    if(this.state.password !== this.state.confirmPassword) {
      this.validationForm.password = true;
      this.validationForm.confirmPassword = true;
    }
    this.forceUpdate();
    if (this.validationForm.email || this.validationForm.name || this.validationForm.password || this.validationForm.confirmPassword) {
      return false;
    } else {
      return true;
    }
  }

  handleChangeGender = event => {    
    this.setState({ gender: event.target.value });
  };

  render() {
    return (
      <Grid className="grid" container item spacing={0} justify="center">
        <Grid item xs={4}>
          <Card>
            <div className="boxStyle  centered-card">
              <h2 className="styleTitle">Nuevo Usuario</h2>
              <form>
                <TextField
                  error={this.validationForm.email}
                  value={this.state.email}
                  onChange={e => this.setState({ email: e.target.value })}
                  id="email-input"
                  label="Correo"
                  className='inputLogin'
                  type="text"
                  autoComplete="email"
                  margin="normal"
                />
                <TextField
                  error={this.validationForm.name}
                  value={this.state.name}
                  onChange={e => this.setState({ name: e.target.value })}
                  id="name-input"
                  label="Nombre Completo"
                  className='inputLogin'
                  type="text"
                  autoComplete="current-name"
                  margin="normal"
                />
                <TextField
                  error={this.validationForm.password}
                  value={this.state.password}
                  onChange={e => this.setState({ password: e.target.value })}
                  id="password-input"
                  label="Password"
                  className='inputLogin'
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                />
                <TextField
                  error={this.validationForm.confirmPassword}
                  value={this.state.confirmPassword}
                  onChange={e => this.setState({ confirmPassword: e.target.value })}
                  id="password2-input"
                  label="Confirmar Password"
                  className='inputLogin'
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                />
                <div className="alignElements">
                  <TextField
                    id="date"
                    onChange={e => this.setState({ birthday: e.target.value })}
                    label="Cumpleaños"
                    type="date"
                    defaultValue="2000-01-01"
                    className="picker-date"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <FormControl component="fieldset" className="radioButtons">
                    <FormLabel component="legend">Genero</FormLabel>
                    <RadioGroup
                      aria-label="Gender"
                      name="gender"
                      value={this.state.gender}
                      onChange={this.handleChangeGender}
                    >
                      <FormControlLabel value="male" control={<Radio color="primary" />} label="Hombre" />
                      <FormControlLabel value="female" control={<Radio color="primary" />} label="Mujer" />
                    </RadioGroup>
                  </FormControl>
                </div>
                <div className="styleTitle">                  
                  <Button onClick={() => this.createUser()} variant="contained" color="primary">
                    Crear Usuario
                  </Button>
                </div>   
              </form>            
            </div>
          </Card>
        </Grid>
      </Grid>
    )
  }
}

export default NewUserForm;
