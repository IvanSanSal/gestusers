import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import './login.css';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      user: "",
      password: ""
    };
  }

  createNewuser() {
    console.log("Crear nuevo usuario")
  }

  connectUser() {
    console.log("El usuario: " + this.state.user + " La password es: " + this.state.password)
  }

  validationLogin(user, password) {

  }

  render() {
    return (
      <Grid className="grid" container item spacing={0} justify="center">
        <Grid item xs={6}>
          <Card>
            <div className="boxStyle  centered-card">
              <h2 className="styleTitle">Iniciar Sesión</h2>
              <form>
                <div className="form-control">
                  <TextField
                    value={this.state.user}
                    onChange={e => this.setState({ user: e.target.value })}
                    id="email-input"
                    label="Correo"
                    className='inputLogin'
                    type="text"
                    autoComplete="current-email"
                    margin="normal"
                  />
                </div>
                <div>
                  <TextField
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                    id="password-input"
                    label="Password"
                    className='inputLogin'
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                  />
                </div>
                <label className="labelCheckBox">
                  <Button color="secondary">Recordar Contraseña</Button>
                </label>
                <div className="divButtonsLogin">
                  <Button component={Link} to="/newUser" onClick={this.createNewuser()} color="primary">Crear Nuevo Usuario</Button>
                  <Button onClick={() => this.connectUser()} variant="contained" color="primary">
                    Acceder
                  </Button>
                </div>
              </form>
            </div>
          </Card>
        </Grid>
      </Grid>
    )
  }
}

export default LoginForm;
