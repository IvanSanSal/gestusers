import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LoginForm from './login';
import NewUserForm from './newUser/newUser';

class RouterLogin extends Component {
  constructor(props) {
    super(props);
      this.paramsRouter = props.paramsRouter
  }

  render() {
    return (
      <BrowserRouter>
          <Switch>
              <Route exact path="/" component={LoginForm} />
              <Route exact path="/newUser" component={NewUserForm} />
          </Switch>
      </BrowserRouter>
    );
  }
}

export default RouterLogin;
